from rest_framework import serializers
from .models import User, Post, Like


class UserSerializer(serializers.ModelSerializer):

    class Meta(object):
        model = User
        fields = ('id', 'email', 'first_name', 'last_name',
                  'date_joined', 'additional_data', 'password')
        read_only_fields = ('date_joined', 'additional_data')
        extra_kwargs = {'password': {'write_only': True}}


class PostSerializer(serializers.ModelSerializer):

    class Meta:
        model = Post
        fields = ('id', 'title', 'author', 'body',)
        read_only_fields = ('created', 'author')


class PostLikeSerializer(serializers.ModelSerializer):
    post = serializers.PrimaryKeyRelatedField(queryset=Post.objects.all())

    class Meta:
        model = Like
        fields = ('id', 'user', 'post')
        read_only_fields = ('user',)


class PostUnLikeSerializer(serializers.ModelSerializer):
    post = serializers.PrimaryKeyRelatedField(queryset=Post.objects.all())

    class Meta:
        model = Like
        fields = ('id', 'user', 'post')
        read_only_fields = ('user',)
