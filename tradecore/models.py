from __future__ import unicode_literals
from django.db import models, transaction
from django.utils import timezone
from django.contrib.auth.models import (
    AbstractBaseUser, PermissionsMixin, BaseUserManager
)
from pyhunter import PyHunter
from core.settings import HUNTER_IO_API_KEY, CLEARBIT_API_KEY
import clearbit
from django.utils import timezone


def mail_verification(mail):
    """Mail validation using hunter.io"""
    try:
        hunter = PyHunter(HUNTER_IO_API_KEY)
        result = hunter.email_verifier(mail)
        if result['result'] == 'undeliverable':
            raise Exception('Mail not approved')
        return True
    except:
        raise


def clearbit_check(email):
    clearbit.key = CLEARBIT_API_KEY
    try:
        data = clearbit.Enrichment.find(email=email, stream=True)
        if data and data['person']:
            return data['person']['name']['fullName']
        else:
            return 'Unknown'
    except:
        raise


class NewUserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email,and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        try:
            if mail_verification(email):
                with transaction.atomic():
                    user = self.model(email=email, additional_data=clearbit_check(email), **extra_fields)
                    user.set_password(password)
                    user.save(using=self._db)
                    return user
        except:
            raise

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        return self._create_user(email, password=password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    """
    email = models.EmailField(max_length=40, unique=True)
    first_name = models.CharField(max_length=30, blank=True)
    last_name = models.CharField(max_length=30, blank=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    additional_data = models.CharField(max_length=128, default='Unknown')
    date_joined = models.DateTimeField(default=timezone.now)

    objects = NewUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    def save(self, *args, **kwargs):
        print('save1')
        if not User.objects.filter(email=self.email).exists():
            print('save2')
            if mail_verification(self.email):
                self.additional_data = clearbit_check(self.email)
        super(User, self).save(*args, **kwargs)
        return self

    def __str__(self):
        return self.email


class Post(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey(User, on_delete=models.PROTECT)
    body = models.TextField()
    created = models.DateTimeField(default=timezone.now(), blank=True)

    def __str__(self):
        return self.title


class Like(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
