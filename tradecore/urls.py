from django.conf.urls import url
from .views import CreateUserAPIView, authenticate_user, CreatePostAPIView, LikePostAPIView, UnLikePostAPIView
from rest_framework_jwt.views import obtain_jwt_token

urlpatterns = [
    url(r'^signup/$', CreateUserAPIView.as_view()),
    url(r'^login/', authenticate_user),
    url(r'^create_post/$', CreatePostAPIView.as_view()),
    url(r'^like_post/$', LikePostAPIView.as_view()),
    url(r'^unlike_post/$', UnLikePostAPIView.as_view()),
]
