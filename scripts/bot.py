import random
import string
import requests
import configparser
from django.db.models import Count
from tradecore.models import User, Post


URL = 'http://127.0.0.1:8000'


def get_config():
    print("get config")
    config_file = 'scripts/config.txt'
    config = configparser.ConfigParser()
    config.read(config_file)
    return {"number_of_users": config.getint('DEFAULT', 'number_of_users'),
            "max_posts_per_user": config.getint('DEFAULT', 'max_posts_per_user'),
            "max_likes_per_user": config.getint('DEFAULT', 'max_likes_per_user')
            }


def random_string(length):
    return ''.join(random.choice(string.ascii_letters) for x in range(length))


def generate_post_body():
    return ' '.join([random_string(8) for i in range(25)])


def generate_random_email(length):
    return random_string(length) + "@gmail.com"


def create_user(email, first_name, last_name, password):
    url = "{url}/tradecore/signup/".format(url=URL)
    response = requests.post(
        url,
        data={'email': email, 'first_name': first_name, 'last_name': last_name, 'password': password}
    )
    if response.status_code == 201:
        return email, password
    else:
        print(response.status_code)


def get_token(email, password):
    url = "{url}/tradecore/login/".format(url=URL)
    response = requests.post(
        url,
        data={'email': email, 'password': password}
    )
    return response.json()['token']


def create_post(title, body, token):
    url = "{url}/tradecore/create_post/".format(url=URL)
    response = requests.post(
        url,
        data={'title': title, 'body': body},
        headers={'Authorization': 'TradeCore ' + token}
    )
    return response.status_code == 201


def like_post(post, token):
    print('like_post')
    url = "{url}/tradecore/like_post/".format(url=URL)
    response = requests.post(
        url,
        data={'post': post},
        headers={'Authorization': 'TradeCore ' + token}
    )
    return response.status_code == 201


def unlike_post(post, token):
    url = "{url}/tradecore/unlike_post/".format(url=URL)
    response = requests.post(
        url,
        data={'post': post,},
        headers={'Authorization': 'TradeCore ' + token}
    )
    return response.status_code == 201


def create_users_and_posts(number_of_users, max_posts_per_user):
    print("create_users_and_posts")
    password = '123456qwer'
    for i in range(number_of_users):
        user = create_user(generate_random_email(random.randint(3, 10)),
                           random_string(random.randint(3, 10)),
                           random_string(random.randint(3, 10)),
                           password)
        for j in range(random.randint(1, max_posts_per_user)):
            token = get_token(user[0], user[1])
            create_post(random_string(random.randint(3, 10)), generate_post_body(), token)


def get_posts(user_id):
    posts = Post.objects.filter(
        author__in=Post.objects.annotate(num_like=Count("like")).filter(num_like=0).values('author_id').distinct()
        ).exclude(author_id=user_id).values('id')
    return posts


def like_activity(max_likes_per_user):
    print("Start of liking")
    password = '123456qwer'
    for user in User.objects.annotate(num_posts=Count('post')).order_by('-num_posts'):
        token = get_token(user.email, password)
        posts = get_posts(user.id)
        if not posts:
            print("Liking is finished")
            break
        for i in range(max_likes_per_user):
            post = random.choice(posts)
            like_post(post['id'], token)
            posts.exclude(id=post['id'])


def run(*args):
    config = get_config()
    create_users_and_posts(config['number_of_users'], config['max_posts_per_user'])
    like_activity(config['max_likes_per_user'])
